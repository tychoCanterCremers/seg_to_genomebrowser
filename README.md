# README #

### What is this repository for? ###
This is a small script that dynamically converts .seg methylation data files into custom genomebrowser track files.

### How do I get set up? ###

Copy the python script to any directory. You can run it from the command line. The script takes 2 commandline arguments. The first argument is the file path to the input .seg file. The second argument is the output file name.