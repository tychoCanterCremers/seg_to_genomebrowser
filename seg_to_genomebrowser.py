import sys
import os
import ntpath


def main():
	seg_file_path = os.path.abspath(sys.argv[1])
	out_file_name = sys.argv[2]
	getBedFile(seg_file_path, out_file_name)


def getBedFile(seg_file_path, out_file_name):
	with open(seg_file_path, "r") as in_file:
		with open(out_file_name + '.track.bed', "w") as out_file:
			data = {}
			ID_list = []
			for line in in_file:
				line = line.strip('\n').split("\t")
				if line[0] != "ID":
					if(line[1]+"-"+line[2] not in data):
						data[line[1]+"-"+line[2]] = {}
					data[line[1]+"-"+line[2]][line[0]] = line[4]
					if line[0] not in ID_list:
						ID_list.append(line[0])
			print("Wrting...")
			out_file.write('''track type=barChart name="barChart Example" description="A barChart file" barChartBars="%s" barChartMetric="Methylation" visibility=pack 
browser position chr1
#chr	start	end	name	score	strand	name2	expCount	expScores	_offset	_lineLength\n''' % (" ".join(ID_list)))
			for key, val in data.items():
				chr = key.split("-")[0]
				pos = key.split("-")[1]
				out_file.write("chr%s\t%s\t%s\t\b\t0\t-\t-\t%i\t" % (chr, pos, pos, len(ID_list)))
				values = [val[id] for id in ID_list]
				out_file.write(",".join(values))
				out_file.write("\n")
			print("done")
			
if __name__ == '__main__':
	main()
